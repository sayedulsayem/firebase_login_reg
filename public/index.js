// Initialize Firebase
var config = {
    apiKey: "AIzaSyBEFngXmEFRvnDRFyaMDznrDTEPKKxJ2yw",
    authDomain: "login-registration-web-app.firebaseapp.com",
    databaseURL: "https://login-registration-web-app.firebaseio.com",
    projectId: "login-registration-web-app",
    storageBucket: "login-registration-web-app.appspot.com",
    messagingSenderId: "517545078659"
};
firebase.initializeApp(config);

//create firebase references
var Auth = firebase.auth();
var dbRef = firebase.database();
var contactsRef = dbRef.ref('contacts');
var usersRef = dbRef.ref('users');
var auth = null;

firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        // User is signed in.

        document.getElementById("user_div").style.display = "block";
        document.getElementById("login_div").style.display = "none";
        document.getElementById("reg_div").style.display = "none";

        var user = firebase.auth().currentUser;

        if(user != null){

            var userId = user.uid;

            return firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {

                var firstName = (snapshot.val() && snapshot.val().firstName) || 'Anonymous';
                document.getElementById("user_fname").innerHTML = "First Name : " + firstName;

                var lastName = (snapshot.val() && snapshot.val().lastName) || 'Anonymous';
                document.getElementById("user_lname").innerHTML = "Last Name : " + lastName;

                var userName = (snapshot.val() && snapshot.val().userName) || 'Anonymous';
                document.getElementById("user_name").innerHTML = "User Name : " + userName;

                var email = (snapshot.val() && snapshot.val().email) || 'Anonymous';
                document.getElementById("user_email").innerHTML = "Email : " + email;

                var phone = (snapshot.val() && snapshot.val().phone) || 'Anonymous';
                document.getElementById("user_phone").innerHTML = "Phone : " + phone;

                var dist = (snapshot.val() && snapshot.val().dist) || 'Anonymous';
                document.getElementById("user_dist").innerHTML = "District : " + dist;

                var subDist = (snapshot.val() && snapshot.val().subDist) || 'Anonymous';
                document.getElementById("user_subdist").innerHTML = "Sub District : " + subDist;

                var zipCode = (snapshot.val() && snapshot.val().zipCode) || 'Anonymous';
                document.getElementById("user_zipcode").innerHTML = "Zip Code : " + zipCode;

            });

        }

    } else {
        // No user is signed in.

        document.getElementById("user_div").style.display = "none";
        document.getElementById("login_div").style.display = "none";
        document.getElementById("reg_div").style.display = "block";

    }
});


function sign(){
    document.getElementById("user_div").style.display = "none";
    document.getElementById("login_div").style.display = "block";
    document.getElementById("reg_div").style.display = "none";
}
function showRegisterForm() {
    document.getElementById("user_div").style.display = "none";
    document.getElementById("login_div").style.display = "none";
    document.getElementById("reg_div").style.display = "block";
}
//register
function register() {

    var userName= document.getElementById("userName").value;
    var firstName= document.getElementById("firstName").value;
    var lastName= document.getElementById("lastName").value;
    var email= document.getElementById("email").value;
    var phone= document.getElementById("phone").value;
    var dist= document.getElementById("district").value;
    var subDist= document.getElementById("subDistrict").value;
    var zipCode= document.getElementById("zipCode").value;
    var password = document.getElementById("password").value;
    var cPassword = document.getElementById("cPassword").value;
    var data= {
        userName: userName,
        firstName: firstName,
        lastName: lastName,
        email: email,
        phone: phone,
        dist: dist,
        subDist: subDist,
        zipCode: zipCode,
    };

    if (email!='' && password!='' && cPassword!=''){
        if (password==cPassword){
            firebase.auth()
                .createUserWithEmailAndPassword(email, password)
                .then(function (user) {
                    //now user is needed to be logged in to save data
                    console.log("Authenticated successfully with payload:", user);
                    auth = user;
                    //now saving the profile data
                    usersRef
                        .child(user.uid)
                        .set(data)
                        .then(function(){
                            console.log("User Information Saved:", user.uid);
                        })
                } )
                .catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // ...
            });
        }
    }
    else {
        if(!email.value && !password.value && !userName.value){
            window.alert("Fill up the form properly.");
        }
        else if(!email.value){
            window.alert("Enter email");
        }
        else if (!password.value){
            window.alert("Enter password");
        }
        else if (password!=cPassword){
            window.alert("Password don't match");
        }
        else if (!userName.value){
            window.alert('Enter a username');
        }
    }
}


function login(){

  var userEmail = document.getElementById("email_field").value;
  var userPass = document.getElementById("password_field").value;
    window.

  firebase.auth().signInWithEmailAndPassword(userEmail, userPass).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;

    window.alert("Error : " + errorMessage);

    // ...
  });

}

function logout(){
  firebase.auth().signOut();
}
